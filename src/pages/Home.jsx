import BasicInfo from "../component/BasicInfo";
import Contact from "../component/Contact";
import Faq from "../component/Faq";
import Hero from "../component/Hero";
import NavBar from "../component/NavBar";
import Services from "../component/Services";

function Home() {
  return (
    <>
      <NavBar />
      <Hero />
      <Services />
      <BasicInfo />
      <Faq />
      <Contact />
    </>
  );
}

export default Home;
