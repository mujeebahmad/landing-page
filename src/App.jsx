import { ThemeProvider } from "styled-components";
import GlobalStyles from './component/styles/GlobalStyles';
import Home from './pages/Home';

function App() {
  const theme = {
    purple: "#4543DE",
    deepPurple: "#252386",
    lightPurple: "#EDECFF",
    yellow: "#FFE6BE",
    white: "#FFFFFF",

  }
  
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles />
        <Home/>
    </ThemeProvider>
  )
}

export default App
