/* eslint-disable no-unused-vars */
/* eslint-disable react/no-unescaped-entities */
import React from "react";
import { StyledBasicInfo } from "./styles/BasicInfo.styled";

export default function BasicInfo() {
  return (
    <StyledBasicInfo>
      <div className="basic_info">
        <h1>
          "<span className="title">RiderExpert</span> has been a game changer
          for my business. Their fast and realiable delivery service has allowed
          me to serve my customers better and increase my sales. I highly
          recommend them!"
        </h1>
        <p>Happy Customer</p>
      </div>
    </StyledBasicInfo>
  );
}
