import { HeroContent, InputWrapper, StyledHero } from "./styles/Hero.styled"

const Hero = () => {
  return (
    <StyledHero>
      <HeroContent>
        <h1>
          RiderExpert: Fast and Reliable Delivery Service
        </h1>
        <p>Get your goods delivered quickly and safely with our efficient motorcycle delivery service.</p>
        <p>Join our waitlist</p>
        <InputWrapper>
          <input type="email" name="email" id="" placeholder="Enter yor email"/>
          <button type="button">Join now</button>
        </InputWrapper>
      </HeroContent>
    </StyledHero>
  )
}

export default Hero