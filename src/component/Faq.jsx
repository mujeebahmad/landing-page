/* eslint-disable react/no-unescaped-entities */
/* eslint-disable no-unused-vars */
import React from "react";
import { StyledFaq } from "./styles/Faq.styled";

export default function Faq() {
  return (
    <StyledFaq>
      <div className="faq_section">
        <p className="title">FAQ's</p>
        <div className="faqs">
          <div className="r1">
            <div className="faq_item">
              <h1>Do you offer customer support?</h1>
              <h3>
                Our delivery service designed to ensure that your goods arrive
                in timely manner and in the best condition possible. We utilze
                motorcyle the fastest mode of transportation in urban area, to
                swiftly navigate through traffic and deliever your items with
                efficiency.
              </h3>
            </div>
            <div className="faq_item">
              <h1>How fast can you deliever my goods?</h1>
              <h3>
                If you have any other questions or need futher assitances,
                please don't hestitate to reach out to our customer support
                team. We're her to help!
              </h3>
            </div>
          </div>

          <div className="r1">
            <div className="faq_item">
              <h1>What areas do you cover?</h1>
              <h3>
                At <span className="brand">RiderExpert</span>, we understand the
                importance of timely and reliable delivery. That is why we
                utilze motorcycles, which allow us to bypass traffic and deliver
                your goods quickly and efficently.
              </h3>
            </div>
            <div className="faq_item">
              <h1>How does your delivery service work?</h1>
              <h3>
                Our delivery covers a wide range of locations within the urban
                area. Whether you need something delievered across town or to a
                neighboring city, we've got you covered
              </h3>
            </div>
          </div>
        </div>
      </div>
    </StyledFaq>
  );
}
