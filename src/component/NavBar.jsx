import {NavLink} from 'react-router-dom';
import { Header, Icons, Nav } from './styles/Navbar.styled';
import { HiMenuAlt3, HiX } from "react-icons/hi";
import { useEffect, useState } from 'react';

const NavBar = () => {
  const [isSticky, setSticky] = useState(false);

  const scrollToSection = (id) => {
    const element = document.getElementById(id);
    if (element) {
      element.scrollIntoView({
        behavior: 'smooth',
        block: 'start',
      });
    }
  };

  useEffect(() => {
    const handleScroll = () => {
      const heroSection = document.getElementById('hero');
      const scrollPosition = window.scrollY;

      if (heroSection) {
        const heroSectionHeight = heroSection.offsetHeight;
        setSticky(scrollPosition > heroSectionHeight);
      }
    };

    window.addEventListener('scroll', handleScroll);

    // Cleanup event listener on component unmount
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  return (
    <>
      <Header className={isSticky ? 'sticky' : ''}>
        <figure>
          <img src="RiderExpert.png" alt="logo" />
        </figure>
        <input type="checkbox" name="" id="check" />
        <Icons htmlFor='check'>
          <HiX id='menu-close'/>
          <HiMenuAlt3 id='menu-open'/>
        </Icons>
        <Nav>
          <NavLink
            style={{'--i':0}}
            onClick={() => scrollToSection('home')}
          >
            Home
          </NavLink>
          <NavLink
            style={{'--i':1}}
            onClick={() => scrollToSection('service')}
          >  
            Service
          </NavLink>
          <NavLink
            style={{'--i':2}}
            onClick={() => scrollToSection('about')}
          >
            About
          </NavLink>
          <NavLink
            style={{'--i':3}}
            onClick={() => scrollToSection('contact')}
          >
            Contact
          </NavLink>
        </Nav>
      </Header>
    </>
  )
}

export default NavBar