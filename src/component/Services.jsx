/* eslint-disable react/no-unescaped-entities */
/* eslint-disable no-unused-vars */
/* eslint-disable react-refresh/only-export-components */
import React from 'react'
import styled, { css } from 'styled-components';

export const InfoContainer = styled.div`
  display: flex;
  padding: 40px;

  @media screen and (max-width: 992px) {
    flex-direction: row;
    padding: 20px;
  }

  @media screen and (max-width: 768px) {
    flex-direction: column;
    padding: 20px;
  }

`;

export const Left = styled.div`
  flex: 3;
  padding: 40px;
  display:flex;
  flex-direction:column;
  justify-content:center;

  @media screen and (max-width: 992px) {
    padding: 40px;
  }

  @media screen and (max-width: 768px) {
    padding: 40px;
    text-align:center;
  }
 
`;

export const Heading = styled.h2`
  font-weight: 800;
  color: #252386;
  font-size: 50px;

  @media screen and (max-width: 992px) {
    font-size:26px;
  }
`;

export const Paragraph = styled.p`
  font-weight: 500;
  color: #4543de;
  font-size: 20px;
  line-height: 1.5;

  @media screen and (max-width: 992px) {
    font-size:20px;
  }
`;

export const Right = styled.div`
  flex: 3;
  padding-left: 90px;


  @media screen and (max-width: 992px) {
    padding-left: 0px;
  }

  @media screen and (max-width: 768px) {
    padding: 10px;
  }

`;

export const RiderImageContainer = styled.div`
  padding-top: 20px;
  position: relative;
  width: 100%; 
  height: auto; 

  @media screen and (max-width: 992px) {
    padding-top: 80px;
    width: 100%; 
    height: 500px; 
  }
`;


export const ServiceContainer = styled.div`
background-color:#4543de;
padding:40px;

  `;
export const MainServices = styled.div`
  padding:50px 80px;
  display:flex;


@media screen and (max-width: 768px) {
  flex-direction: column;
  padding: 20px;
  text-align:center;
}

  `;

  export const ServiceHeading = styled.h2`
  font-weight: 800;
  color: #ffffff;
  font-size: 50px;
  flex:1;

  @media screen and (max-width: 992px) {
    font-size:24px;
    padding-top:20px
  }
`;

export const ServiceParagraph = styled.p`
font-weight: 500;
color: #ffffff;
font-size: 20px;
line-height: 1.5;
flex:2;

@media screen and (max-width: 992px) {
  font-size:16px;
}
`;

export const FoodDelivery = styled.div`
display: flex;


@media screen and (max-width: 768px) {
  flex-direction: column;
  padding: 20px;
  text-align:center;
}


`;

export const TrafficImageContainer = styled.div`
  padding-top: 20px;
  position: relative;
  width: 100%; 
  height: auto; 
`;


export const RightDelivery = styled.div`
  flex: 3;
  display:flex;
  flex-direction:column;
  justify-content:center;
`;

export const DeliveryHeading = styled.h2`
  font-weight: 800;
  color: gold;
  font-size: 30px;
`;

export const DeliveryParagraph = styled.p`
  font-weight: 500;
  color: #ffffff;
  font-size: 20px;
  line-height: 1.5;


@media screen and (max-width: 992px) {
  font-size:16px;
}
`;

export const DocumentDelivery = styled.div`
  display:flex;


@media screen and (max-width: 768px) {
  flex-direction: column;
  padding: 20px;
  text-align:center;
}
`;

export const DocumentHeading = styled.h2`
  font-weight: 800;
  color: gold;
  font-size: 30px;
`;

export const AdditionalServices = styled.div`
 background-color: #ffffff;
padding-top: 40px
`;
export const Title = styled.h2`
  color: #4543de;
  font-size : 50px;
  text-align:center;
  font-weight: 900;
  
`;
export const Flex3 = styled.div`
  display: flex;
  
  @media screen and (max-width: 768px) {
    flex-direction: column;
    padding: 20px;
  }
`;
export const LeftContent = styled.div`
padding:20px 40px;
text-align:center;

`;
export const LeftHeading = styled.h2`
color: #252386;
font-weight:900;
`;

export const LeftParagraph = styled.p`
color: #4543de;
font-size: 18px;
line-height:1.5;
;
`;

  function Services() {
  return (
    <>
      <InfoContainer>
      <Left>
        <Heading>Delivering Excellence</Heading>
        <Paragraph>Welcome to RiderExpert, the premier delivery service in the urban area. We specialize in providing fast and reliable delivery of goods using motocycles, ensuring that your items arrive on time and in the best condition possible. With our convenient pickup and delivery service, all you need to do is provide us with the locations, and we'll take care of the rest </Paragraph>
      </Left>
      <Right>
        <RiderImageContainer>
         <img src='delivery_rider.jpg' alt='rider' style={{ width: '100%', height: '100%', objectFit: 'cover'}}/>
        </RiderImageContainer>
      </Right>
    </InfoContainer>

    <ServiceContainer>
      <MainServices>
        <ServiceHeading>Our Services</ServiceHeading>
        <ServiceParagraph>Welcome to RiderExpert, your trusted delivery service in the urban area. We specialize in providing fast and efficient delivery solutions for businesses and individuals. Our team of skilled riders is dedicated to ensuring that your goods are delivered in a timely manner and in the best codition possible. Whether you need to send a package across town or have important document elivered, we've got you covered. Experience the convenience of our motocycle delivery service today</ServiceParagraph>
      </MainServices>   

    <FoodDelivery>
      <Left>
        <TrafficImageContainer>
         <img src='delivery_traffic.jpg' alt='rider' style={{ width: '100%', height: '500px', objectFit: 'cover', borderRadius: '10px' }}  />
        </TrafficImageContainer>
      </Left>
      <RightDelivery>
      <DeliveryHeading>Food delivery</DeliveryHeading>
      <DeliveryParagraph>Our first service is focused on providing quick and reliable food delivery. We understand that you want your food to arrive hot and fresh, which is why we use motocycles to ensure fast and efficient delivery. Whether you're craving a pizza, burger, or sushi, our experienced riders will deliver your favorite dishes right to your door. Enjoy the convenience of restuarant-quality food deivered to your doorstep</DeliveryParagraph>
      </RightDelivery>
    </FoodDelivery>

    <DocumentDelivery>
      <Left>
        <DocumentHeading>Document and package delivery</DocumentHeading>
        <DeliveryParagraph>Welcome to RiderExpert, the premier delivery service in the urban area. We specialize in providing fast and reliable delivery of goods using motocycles, ensuring that your items arrive on time and in the best condition possible. With our convenient pickup and delivery service, all you need to do is provide us with the locations, and we'll take care of the rest </DeliveryParagraph>
      </Left>
      <Right>
        <RiderImageContainer>
         <img src='delivery_rider.jpg' alt='rider'  style={{ width: '100%', height: '100%', objectFit: 'cover'}}/>
        </RiderImageContainer>
      </Right>
    </DocumentDelivery>
  </ServiceContainer>

  <AdditionalServices>
    <Title>Additional Services</Title>
    <Flex3>
      <LeftContent>
        <LeftHeading>Fast and Reliable Motoycle Delivery</LeftHeading>
        <LeftParagraph>At RiderExpert, we offer a premium delivery service for valuable and dedicated items. Our trained riders are equipped with the necessary tools and packaging materials to ensure that your goods are handled with utmost care and delivered safely. Rest easy knowing that your precious items are in good hands</LeftParagraph>
      </LeftContent>
      <LeftContent>
        <LeftHeading>Express Delivery</LeftHeading>
        <LeftParagraph>With our fast and reliable motocycle delivery service, we ensure that your goods are transported quickly and efficiently to their destination. Our experienced riders are skilled at navigating through traffic and delivering your items in the best condition possible. Trust us to handle your deliveries with care and professionalism</LeftParagraph>
      </LeftContent>
      <LeftContent>
        <LeftHeading>Premium Delivery Service</LeftHeading>
        <LeftParagraph>Our express delivery service is perfect for those urgent deliveries that need to be made as quickly as possible. Whether it's an important document or a last-minuit gift, we guarantee timely delivery to your desired location. Count on us to get your items delivered on time, every time.</LeftParagraph>
      </LeftContent>
    </Flex3>
  </AdditionalServices>

    </>
   
  )
}

export default Services
