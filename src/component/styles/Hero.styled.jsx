import styled from "styled-components";

export const StyledHero = styled.section`
  position: relative;
  width: 100%;
  background-image: url("delivery_rider1.jpg");
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center center;
  background-attachment: fixed;
  color: ${({ theme }) => theme.lightPurple};

  &::before{
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0,0,0,.5);
  }
`;

export const HeroContent = styled.div`
  position: relative;
  width: 60%;
  margin: auto;
  padding: 1rem;
  color: ${({ theme }) => theme.lightPurple};
  text-align: center;

  h1{
    padding-top: 12rem;
    font-size: clamp(3rem, 5vw, 6rem);
  }

  p{
    margin-top: 1.5rem;
    font-size: clamp(1.6rem, 2.5vw, 3rem);
    font-weight: 400;
  }

  @media (max-width: 640px){
    width: 90%;
  }
`;


export const InputWrapper = styled.div`
  width: 80%;
  margin: 1rem auto 3rem auto;

  input{
    width: 70%;
    border: none;
    outline: none;
    padding: 1rem;
  }

  button{
    border: none;
    padding: 1rem;
    color: ${({ theme }) => theme.purple};
    background-color: ${({ theme }) => theme.lightPurple};
    cursor: pointer;
    
    &:hover{
      color: ${({ theme }) => theme.lightPurple};
      background-color: ${({ theme }) => theme.purple};
    }
  }
  
  @media (max-width: 480px) {
    width: 100%;   
  }
`;