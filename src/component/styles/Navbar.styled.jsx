import styled from "styled-components";

export const Header = styled.header`
  display: flex;
  width: 100%;
  max-width: 140rem;
  height: 5.6rem;
  justify-content: space-between;
  align-items: center;
  padding: 2rem;
  position: relative;
  z-index: 1000;

  &.sticky {
    position: sticky;
    top: 0;
  }


  &::before{
    content: "";
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    z-index: -1;
    background: rgba(255,255,255,.8);
    backdrop-filter: blur(5rem);
  }
  
  &::after{
    content: "";
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: -100%;
    background: linear-gradient(90deg, transparent, rgba(255,255,255,.4), transparent);
    transition: .5s;
  }

  &:hover::after{
    left: 100%
  }

  figure{
    display: flex;
    align-items: center;
    justify-content: center;
  }

  img{
    width: auto;
    height: 2.4rem;
  }

  #check{
    display:none;
  }

  @media (max-width: 480px) {
    #check:checked~label #menu-open{
      display: none;
    }   
    #check:checked~label #menu-close{
      display: block;
    }
    
    #check:checked~nav{
      height: 22rem;
    }
    
    #check:checked~nav a{
      transform: translateY(0);
      opacity: 1;
      transition-delay: calc(.15s * var(--i));
    }
  }

`

export const Nav = styled.nav`
  display: flex;
  align-items: center;
  gap: 2rem;
  font-size: 1.4rem;

  a{
    text-decoration: none;
    font-size: 1.4rem;
    font-weight: 400;
    transition: all 100ms ease-in-out 0s;
    cursor: pointer;
    transform: scale(1);
    color: black;
    
    &:hover {
    color: ${({ theme }) => theme.purple};
    border-bottom: 2px solid ${({ theme }) => theme.purple};

    }
  }

  @media (max-width: 480px) {
    position: absolute;
    top: 100%;
    left: 0;
    width: 100%;
    height: 0;
    background: rgba(255,255,255,.8);
    backdrop-filter: blur(5rem);
    flex-direction: column;
    box-shadow: .1rem .1rem .1rem 0rem rgba(255,255,255,.8);
    overflow: hidden;
    transition: .3s ease;

    a {
      margin-top: 1.5rem;
      transform: translateY(-5rem);
      opacity: 0;
      transition: .3s ease;
    }
  }

`


export const Icons = styled.label`
  position: absolute;
  right: 2rem;
  font-size: 2.8rem;
  color: ${({ theme }) => theme.purple};
  cursor: pointer;
  display: none;


  @media (max-width: 480px) {
    display: inline-flex;
    
    #menu-close{
      display: none;
    }

  }
`
