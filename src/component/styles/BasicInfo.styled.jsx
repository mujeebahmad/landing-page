import styled from "styled-components";

export const StyledBasicInfo = styled.div`
  // basic info styles
  .basic_info {
    text-align: center;
    color: white;
    font-weight: 400;
    padding: 120px;
    background-image: url("delivery_receipt.jpg");
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center center;
    background-attachment: fixed;
    background-color: rgba(0, 0, 255, 0.5);
    background-attachment: fixed;
    background-blend-mode: multiply;

    .title {
      border-bottom: dashed 3px red;
    }

    h1{
      font-size: 25px !important;
    }

    p{
      margin-top: 10px;
      font-size: 15px;
    }
  }
`;
