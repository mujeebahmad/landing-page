import styled from "styled-components";

export const StyledContact = styled.div`
  // contact styles
  .contact_section {
    background-color: #252386;
    color: white;
    font-size: 20px !important;
    padding: 100px;

    form {
      max-width: 900vw;
      margin: 20px auto;
      font-size: 12px;
      background-color: white;
      padding: 30px;
      display: flex;
      flex-direction: column;
      justify-content: flex-start;
    }

    textarea {
      background-color: #edecff;
      border-radius: 5px;
      width: 90vw;
      margin: 10px;
      border: 1px solid #4543de;
    }

    .formRow {
      display: flex;
      flex-direction: row;
    }
    input {
      width: 100%;
      margin: 10px;
      border-radius: 5px;
      padding: 18px;
      margin-bottom: 16px;
      box-sizing: border-box;
      background-color: #edecff;
      border: 1px solid #4543de;

    }

    input::placeholder {
      color: #4543de;
    }

    textarea::placeholder {
      margin: 10px;
      padding: 18px;
      color: #4543de;
    }

    textarea:focus {
      border: 1px solid #4543de;
      outline: none;
    }

    input:focus {
      border: 1px solid #4543de;
      outline: none;
    }

    button {
      background-color: #4543de;
      color: white;
      border-radius: 5px;
      border: 1px solid #252386;
      height: 40px;
      width: 90px;
      cursor: pointer;
      margin: 10px;
    }
  }
`;
