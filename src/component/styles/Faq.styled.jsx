import styled from "styled-components";

export const StyledFaq = styled.div`
  // faq styles
  .faq_section {
    display: flex;
    flex-direction: column;
    justify-content: centre;
    align-items: center;
    text-align: left;
    padding: 50px;
    color: #252386;
    font-size: 12px !important;

    .title {
      font-weight: bolder;
      font-size: 30px;
      color: #4543de;
    }

    .faqs {
      display: flex;
      flex-direction: row;
      justify-content: center;
      align-items: flex-start;

      .r1 {
        width: 600px;
        padding: 20px;
        display: flex;
        flex-direction: column;

        .faq_item {
          margin-bottom: 35px;
          line-height: 30px;
        }

        .brand {
          border-bottom: dashed 3px red;
        }
      }
    }

    @media (max-width: 640px) {
      .faq_section{
        overflow-x: hidden !important;
      }
      .faqs {
        display: flex;
        flex-direction: column !important;
        justify-content: center;
        align-items: center;
        width: 100vw;
      }
    }
  }
`;
