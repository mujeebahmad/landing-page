/* eslint-disable no-unused-vars */
import React from "react";
import { StyledContact } from "./styles/Contact.styled";

export default function Contact() {
  return (
    <StyledContact>
      <div className="contact_section">
        <h2>Contact us today</h2>
        <h5>
          Contact us today to experience our reliable and efficient delivery
          service. We are dedicated to ensuring that your goods are delivered in
          a timely manner and in the best condition possible. Simply provide us
          with the pick up and delivery locations, and we will take care of the
          rest. Trust <span className="brand">RiderExpert</span>to get your
          goods where they need to be, quickly and securely.
        </h5>
        <form>
          <div className="formRow">
            {" "}
            <input
              type="text"
              id="firstName"
              name="firstName"
              placeholder="First Name"
              required
            />
            <input type="text" id="lastName" name="lastName" placeholder="Last Name" required />
          </div>
          <div className="formRow">
            <input type="email" id="email" name="email"  placeholder="Email" required />

            <input type="tel" id="phone" name="phone" placeholder="Phone Number" required />
          </div>
          <div className="formRow">
            <textarea
              id="message"
              name="message"
              rows="8"
              cols="140"
              placeholder="Message"
              required
            ></textarea>
          </div>
          <button type="submit">Submit</button>
        </form>
      </div>
    </StyledContact>
  );
}
